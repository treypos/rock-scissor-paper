loop do

print "(R)ock, (S)cissor, (P)aper? "
c = gets.strip.capitalize

if c == "R"
    user_choice = :rock
elsif c == "S"
    user_choice = :scissor
elsif c == "P"
    user_choice = :paper
else
    puts "Can't understand what you want, sorry... :("
    break
end

arr = [:rock, :scissor, :paper]
computer_choice = arr[rand(0..2)]

puts "Computer choice #{computer_choice}"

if computer_choice == user_choice
    puts "Nobody win"
elsif computer_choice == :rock && user_choice == :paper
    puts "You win"
elsif computer_choice == :rock && user_choice == :scissor
    puts "Computer win"
elsif computer_choice == :scissor && user_choice == :paper
    puts "Computer win"
elsif computer_choice == :scissor && user_choice == :rock
    puts "You win"
elsif computer_choice == :paper && user_choice == :rock
    puts "Computer win"
else computer_choice == :paper && user_choice == :scissor
    puts "You win"
end
end